from django.contrib import admin

# Register your models here.

from.models import *

@admin.register(City)
class CityAdmin(admin.ModelAdmin):
    list_display = [
        "name1",
    ]

@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = [
        "name2",
        "description",      
    ]

@admin.register(Stadium)
class StadiumAdmin(admin.ModelAdmin):
    list_display = [
        "name3",
        "description2", 
        "location",     
    ]
