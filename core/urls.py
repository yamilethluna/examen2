from django.urls import path
from core import views



app_name = "core"


urlpatterns = [
    path('list1/city/', views.List1.as_view(), name="list1"),
    path('create1/city/',views.CreateCity.as_view(), name="create1"),
    path('detail/city/<int:pk>/', views.DetailCity.as_view(), name= "detail1"),
    path('update/city/<int:pk>/', views.UpdateCity.as_view(), name= "update1"),
    path('delete/city/<int:pk>/', views.DeleteCity.as_view(), name= "delete1"),

    path('list2/team/', views.List2.as_view(), name="list2"),
    path('create2/team/',views.CreateTeam.as_view(), name="create2"),
    path('detail/team/<int:pk>/', views.DetailTeam.as_view(), name= "detail2"),
    path('update/team/<int:pk>/', views.UpdateTeam.as_view(), name= "update2"),
    path('delete/team/<int:pk>/', views.DeleteTeam.as_view(), name= "delete2"),

    path('list3/stadium/', views.List3.as_view(), name="list3"),
    path('create3/stadium/',views.CreateStadium.as_view(), name="create3"),
    path('detail/stadium/<int:pk>/', views.DetailStadium.as_view(), name= "detail3"),
    path('update/stadium/<int:pk>/', views.UpdateStadium.as_view(), name= "update3"),
    path('delete/stadium/<int:pk>/', views.DeleteStadium.as_view(), name= "delete3"),
    
]
