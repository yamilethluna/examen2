from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .models import *
from .forms import *
# Create your views here.

###### C I T Y
class CreateCity(generic.CreateView):
    template_name = "core/create1.html"
    model= City
    form_class = Cityform
    success_url = reverse_lazy("core:list1")



class List1(generic.View):
    template_name = "core/list1.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = City.objects.filter()
        self.context = {
            "city": queryset
        }
        return render(request, self.template_name, self.context)

class UpdateCity(generic.UpdateView):
    template_name = "core/update1.html"
    model = City
    form_class = UpdateCityform
    success_url = reverse_lazy("core:list1")

   
class DeleteCity(generic.DeleteView):
    template_name = "core/delete1.html"
    model = City
    success_url = reverse_lazy("core:list1")

class DetailCity(generic.DetailView):
    template_name = "core/detail1.html"
    model = City

### T E A M S
class CreateTeam(generic.CreateView):
    template_name = "core/create2.html"
    model= Team
    form_class = Teamform
    success_url = reverse_lazy("core:list2")



class List2(generic.View):
    template_name = "core/list2.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Team.objects.filter()
        self.context = {
            "team": queryset
        }
        return render(request, self.template_name, self.context)

class UpdateTeam(generic.UpdateView):
    template_name = "core/update2.html"
    model = Team
    form_class = UpdateTeamform
    success_url = reverse_lazy("core:list2")

   
class DeleteTeam(generic.DeleteView):
    template_name = "core/delete2.html"
    model = Team
    success_url = reverse_lazy("core:list2")

class DetailTeam(generic.DetailView):
    template_name = "core/detail2.html"
    model = Team

###STADIUMS
class CreateStadium(generic.CreateView):
    template_name = "core/create3.html"
    model= Stadium
    form_class = Stadiumform
    success_url = reverse_lazy("core:list3")



class List3(generic.View):
    template_name = "core/list3.html"
    context = {}

    def get(self, request, *args, **kwargs):
        queryset = Stadium.objects.filter()
        self.context = {
            "stadium": queryset
        }
        return render(request, self.template_name, self.context)

class UpdateStadium(generic.UpdateView):
    template_name = "core/update3.html"
    model = Stadium
    form_class = UpdateStadiumform
    success_url = reverse_lazy("core:list3")

   
class DeleteStadium(generic.DeleteView):
    template_name = "core/delete3.html"
    model = Stadium
    success_url = reverse_lazy("core:list3")

class DetailStadium(generic.DetailView):
    template_name = "core/detail3.html"
    model = Stadium