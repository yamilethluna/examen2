from django import forms
from .models import *


#CITIES

class Cityform(forms.ModelForm):
    class Meta:
        model = City
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Whrite name of the City"}),
            }

class UpdateCityform(forms.ModelForm):
    class Meta:
        model = City
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Whrite name of the City"}),
            }
#TEAMS
class Teamform(forms.ModelForm):
    class Meta:
        model = Team
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Whrite name of the team"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"whhrite the description"}),
            }
        
class UpdateTeamform(forms.ModelForm):
    class Meta:
        model = Team
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Whrite name of the team"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"whhrite the description"}),
            }

#STADIUMS
class Stadiumform(forms.ModelForm):
    class Meta:
        model = Stadium
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Whrite name of the team"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"whhrite the description"}),
            "location": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"whhrite the description"}),

            }
        
class UpdateStadiumform(forms.ModelForm):
    class Meta:
        model = Stadium
        fields = "__all__"
        widgets = {
            "name": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Whrite name of the team"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"whhrite the description"}),
            "location": forms.Textarea(attrs={"type":"text", "class":"form-control", "rows":3, "placeholder":"whhrite the description"}),
            }
