from django.db import models
from django.contrib.auth.models import User
from datetime import timedelta
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.

# @receiver(post_save, sender=GrantGoal)
# def end_time_grantgoal(sender, instance, **kwargs):
#     if instance.final_date is None or instance.final_date=='':
#         instance.final_date = instance.timestamp + timedelta(days=instance.days_duration)
#         instance.save()

#### ISSUES ####

class City(models.Model):
    name1 = models.CharField(max_length=128, default="city name")

    def __str__(self):
        return self.name1


class Team(models.Model):
    name2 = models.CharField(max_length=128, default="team name")
    description = models.CharField(max_length=256, default="team Description")
  
    def __str__(self):
        return self.name2
    
class Stadium(models.Model):
    name3 = models.CharField(max_length=128, default="stadium name")
    description2 = models.CharField(max_length=256, default="stadium Description")
    location = models.CharField(max_length=256, default="stadium location")

  
    def __str__(self):
        return self.name3
      

