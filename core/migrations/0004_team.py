# Generated by Django 4.2.7 on 2023-11-05 22:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_delete_grantgoal'),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name2', models.CharField(default='team name', max_length=128)),
                ('description', models.CharField(default='team Description', max_length=256)),
                ('contact', models.CharField(default='team contact', max_length=256)),
            ],
        ),
    ]
